
"""
this is a library to be tested
"""

from dataclasses import dataclass


def sub(a: int, b:int ) -> int:
    assert isinstance(a, int)
    assert isinstance(b, int)
    return a - b


@dataclass
class Employee:
    name: str
    surname: str

    @property
    def mail(self):
        return f"{self.name}.{self.surname}"

