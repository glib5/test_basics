import unittest
# also supports mocking

import calc


class TestCalc(unittest.TestCase):

    def test_sub(self):
        self.assertEqual(calc.sub(10, 5), 5)
        self.assertEqual(calc.sub(0, 0), 0)
        self.assertEqual(calc.sub(-5, 5), -10)
        self.assertEqual(calc.sub(1, 1), 0) 

    def test_sub_wrong_args(self):
        self.assertRaises(AssertionError, calc.sub, a=1.0, b=0)


# better to place this into another file
class TestEmployee(unittest.TestCase):

    # this will be called at the beginning of each test_ method
    def setUp(self) -> None:
        self.employee = calc.Employee("name", "surname")

    def test_email(self):
        self.assertEqual(self.employee.mail, "name.surname")


if __name__ == "__main__":
    unittest.main()
